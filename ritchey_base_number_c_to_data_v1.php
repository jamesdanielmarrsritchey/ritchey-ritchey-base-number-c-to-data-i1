<?php
#Name:Ritchey Base Number C To Data v1
#Description:Convert a Ritchey Base Number C to a data string using the Ritchey Base Number C decoding scheme. Returns string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'number' is a Ritchey Base Number C. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):number:number:required,display_errors:bool:optional
#Content:
if (function_exists('ritchey_base_number_c_to_data_v1') === FALSE){
function ritchey_base_number_c_to_data_v1($number, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@ctype_digit($number) === FALSE){
		$errors[] = "number - not a number";
	}
	###Ensure the number is even, because Ritchey Base Number Cs are always even!
	if (@strpbrk(substr(strlen($number), -1), 13579) == TRUE){
		$errors[] = "number - odd number";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Convert number to an array of 4 digit sets. Change each 4 digit set into a Base64 character. Convert array to string. Decode Base64. Write data to destination. NOTE: Base64 can encode/decode in chunks of 3 bytes and still achieve the same result as processing data all at once, but this implementation does not make use of this. There is no point since it receives the number as a variable not in a file.]
	if (@empty($errors) === TRUE){
		###Convert number to an array of 4 digit sets
		$number = @str_split($number, 4);
		###Convert each number set to a base64 character.
		foreach ($number as &$character){
			if ($character === "2222"){
				$character = 'A';	
			} else if ($character === "2224"){
				$character = 'B';
			} else if ($character === "2226"){
				$character = 'C';
			} else if ($character === "2228"){
				$character = 'D';
			} else if ($character === "2242"){
				$character = 'E';
			} else if ($character === "2244"){
				$character = 'F';
			} else if ($character === "2246"){
				$character = 'G';
			} else if ($character === "2248"){
				$character = 'H';
			} else if ($character === "2262"){
				$character = 'I';	
			} else if ($character === "2264"){
				$character = 'J';	
			} else if ($character === "2266"){
				$character = 'K';	
			} else if ($character === "2268"){
				$character = 'L';	
			} else if ($character === "2282"){
				$character = 'M';	
			} else if ($character === "2284"){
				$character = 'N';	
			} else if ($character === "2286"){
				$character = 'O';	
			} else if ($character === "2288"){
				$character = 'P';	
			} else if ($character === "2422"){
				$character = 'Q';	
			} else if ($character === "2424"){
				$character = 'R';	
			} else if ($character === "2426"){
				$character = 'S';	
			} else if ($character === "2428"){
				$character = 'T';	
			} else if ($character === "2442"){
				$character = 'U';	
			} else if ($character === "2444"){
				$character = 'V';	
			} else if ($character === "2446"){
				$character = 'W';	
			} else if ($character === "2448"){
				$character = 'X';	
			} else if ($character === "2462"){
				$character = 'Y';	
			} else if ($character === "2464"){
				$character = 'Z';	
			} else if ($character === "2466"){
				$character = 'a';	
			} else if ($character === "2468"){
				$character = 'b';
			} else if ($character === "2482"){
				$character = 'c';
			} else if ($character === "2484"){
				$character = 'd';
			} else if ($character === "2486"){
				$character = 'e';
			} else if ($character === "2488"){
				$character = 'f';
			} else if ($character === "2622"){
				$character = 'g';
			} else if ($character === "2624"){
				$character = 'h';
			} else if ($character === "2626"){
				$character = 'i';	
			} else if ($character === "2628"){
				$character = 'j';	
			} else if ($character === "2642"){
				$character = 'k';	
			} else if ($character === "2644"){
				$character = 'l';	
			} else if ($character === "2646"){
				$character = 'm';	
			} else if ($character === "2648"){
				$character = 'n';	
			} else if ($character === "2662"){
				$character = 'o';	
			} else if ($character === "2664"){
				$character = 'p';	
			} else if ($character === "2666"){
				$character = 'q';	
			} else if ($character === "2668"){
				$character = 'r';	
			} else if ($character === "2682"){
				$character = 's';	
			} else if ($character === "2684"){
				$character = 't';	
			} else if ($character === "2686"){
				$character = 'u';	
			} else if ($character === "2688"){
				$character = 'v';	
			} else if ($character === "2822"){
				$character = 'w';	
			} else if ($character === "2824"){
				$character = 'x';	
			} else if ($character === "2826"){
				$character = 'y';	
			} else if ($character === "2828"){
				$character = 'z';	
			} else if ($character === "2842"){
				$character = '0';	
			} else if ($character === "2844"){
				$character = '1';	
			} else if ($character === "2846"){
				$character = '2';	
			} else if ($character === "2848"){
				$character = '3';	
			} else if ($character === "2862"){
				$character = '4';	
			} else if ($character === "2864"){
				$character = '5';	
			} else if ($character === "2866"){
				$character = '6';	
			} else if ($character === "2868"){
				$character = '7';	
			} else if ($character === "2882"){
				$character = '8';	
			} else if ($character === "2884"){
				$character = '9';	
			} else if ($character === "2886"){
				$character = '+';	
			} else if ($character === "2888"){
				$character = '/';	
			} else if ($character === "4222"){
				$character = '=';	
			} else {
				$errors[] = "task - Convert numbers to characters";
				goto result;
			}
		}
		###Convert array back to string.
		$number = @implode($number);
		###Decode base64
		$number = @base64_decode($number);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('ritchey_base_number_c_to_data_v1_format_error') === FALSE){
			function ritchey_base_number_c_to_data_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("ritchey_base_number_c_to_data_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return $number;
	} else {
		return FALSE;
	}
}
}
?>